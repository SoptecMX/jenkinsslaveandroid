# README #

# JenkinsSlaveAndroid #
## Specs ##
- Android APIs (15,19,21,22,23,24)
- Android Extras (ALL)
- Android Tools
  - Android SDK Platform-tools
	- 15
	- 19
	- 21
	- 22
	- 23
	- 24
    - 25
  - Android SDK Build-tools
    - 19.1
    - 20
    - 21.1.2
    - 22.0.1
    - 23.0.1
    - 23.0.2
    - 24.0.1
    - 25
    - 25.0.2
  - Android Sources
    - 15
    - 19
    - 21
    - 22
    - 23
    - 24
    - 25
## Run ##
``docker run -d <IMAGE> -url http://<MASTERURL>:<PORT>/ -workDir "<WORKDIR>" <SECRETKEY> "<SLAVENAME>"``