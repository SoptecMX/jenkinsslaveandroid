FROM jenkins/slave:3.16-1
MAINTAINER Brandom Marañon <brandommario@gmail.com>

ENV ANDROID_HOME=$HOME/.android
ENV PATH=$PATH:$ANDROID_HOME/tools/bin

COPY jenkins-slave /usr/local/bin/jenkins-slave
COPY sdk-tools-linux-3859397.zip $HOME/

RUN unzip -q $HOME/sdk-tools-linux-3859397.zip -d ${ANDROID_HOME} \
    && touch ${ANDROID_HOME}/repositories.cfg \   
    && yes | sdkmanager --licenses \
    && sdkmanager --verbose \
    "add-ons;addon-google_apis-google-15" \
    "add-ons;addon-google_apis-google-19" \
    "add-ons;addon-google_apis-google-21" \
    "add-ons;addon-google_apis-google-22" \
    "add-ons;addon-google_apis-google-23" \
    "add-ons;addon-google_apis-google-24" \
    "platforms;android-15" \
    "platforms;android-19" \
    "platforms;android-21" \
    "platforms;android-22" \
    "platforms;android-23" \
    "platforms;android-24" \
    "platforms;android-25" \
    "sources;android-15" \
    "sources;android-19" \
    "sources;android-21" \
    "sources;android-22" \
    "sources;android-23" \
    "sources;android-24" \
    "sources;android-25" \
    "build-tools;19.1.0" \
    "build-tools;20.0.0" \
    "build-tools;21.1.2" \
    "build-tools;22.0.1" \
    "build-tools;23.0.1" \
    "build-tools;23.0.2" \
    "build-tools;24.0.1" \
    "build-tools;25.0.0" \
    "build-tools;25.0.2" \
    "build-tools;26.0.0" \
    "extras;android;gapid;1" \
    "extras;android;gapid;3" \
    "extras;android;m2repository" \
    "extras;google;auto" \
    "extras;google;google_play_services" \
    "extras;google;instantapps" \
    "extras;google;m2repository"\
    "extras;google;market_apk_expansion" \
    "extras;google;market_licensing" \
    "extras;google;simulators" \
    "extras;google;webdriver" \
    && rm -rf $HOME/sdk-tools-linux-3859397.zip

ENTRYPOINT ["jenkins-slave"]